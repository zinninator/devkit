<?php
namespace DevKit\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \DevKit\Model\Table\ContactsTable&\Cake\ORM\Association\BelongsTo $Contacts
 * @property \DevKit\Model\Table\UserGroupsTable&\Cake\ORM\Association\BelongsTo $UserGroups
 * @property \DevKit\Model\Table\EmployeesTable&\Cake\ORM\Association\HasMany $Employees
 * @property \DevKit\Model\Table\NotesTable&\Cake\ORM\Association\HasMany $Notes
 *
 * @method \DevKit\Model\Entity\User get($primaryKey, $options = [])
 * @method \DevKit\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \DevKit\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \DevKit\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \DevKit\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \DevKit\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \DevKit\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \DevKit\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('DevKit.Contacts');
        $this->belongsTo('DevKit.UserGroups');

        $this->hasMany('DevKit.Employees');
        $this->hasMany('DevKit.Notes');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->email('email')
            ->notEmptyString('email')
            ->add('email', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('password')
            ->sameAs('password', 'password_confirmation', 'Passwords do not match')
            ->maxLength('password', 100)
            ->allowEmptyString('password');

        $validator
            ->boolean('verified')
            ->allowEmptyString('verified');

        $validator
            ->boolean('deleted')
            ->notEmptyString('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['contact_id'], 'Contacts'));
        $rules->add($rules->existsIn(['user_group_id'], 'UserGroups'));

        return $rules;
    }
}
